import React from "react";
import { connect } from "react-redux";
import { Jumbotron, Container, Card } from "reactstrap";
import { faCircle, faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Link } from "react-router-dom";

const mapStateToProps = (state) => {
  let { loggedUser, eventTypes } = state;

  return { loggedUser, eventTypes };
};

/*
const mapDispatchToProps = (dispatch) => {
  return {
    loadEventTypes: (events) => {
      console.log("trigger load events");

      dispatch({ type: LOAD_EVENT_TYPES, payload: events });
    },
  };
};
*/

const Scheduler = (props) => {
  console.log(props.loggedUser);
  console.log(props.eventTypes);

  return (
    <div className="main">
      <Container style={{ margin: "10px 10px" }}>
        <h3>{props.loggedUser.name}</h3>
        <p>
          Welcome to my scheduling page. Please follow the instructions to add
          an event to my calender.
        </p>
        <Card style={{ width: "24rem", border: "none" }}>
          <ul>
            {props.eventTypes.map((et, index) => (
              <div key={index}>
                <hr />
                <li>
                  <p
                    style={{ display: "flex", justifyContent: "space-between" }}
                  >
                    <span>
                      <FontAwesomeIcon
                        icon={faCircle}
                        style={{ color: "#7B68EE", marginRight: "10px" }}
                      />
                      {et.title}
                    </span>
                    <span>
                      {" "}
                      <Link
                        to={{
                          pathname: `${props.location.pathname}/panel`,
                          user: props.loggedUser.mail,
                          duration: et.duration,
                        }}
                      >
                        <FontAwesomeIcon icon={faCaretRight} />
                      </Link>
                    </span>
                  </p>
                </li>
              </div>
            ))}
          </ul>
        </Card>
      </Container>
    </div>
  );
};

export default connect(mapStateToProps)(Scheduler);
