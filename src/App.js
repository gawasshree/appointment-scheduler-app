import React, { Component } from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { connect } from "react-redux";

import Login from "./Login";
import Signup from "./Signup";
import ResetPassword from "./ResetPassword";
import UpdatePassword from "./UpdatePassword";
import Schedule from "./Schedule";
import EditEventType from "./EventType/EditEventType";

import Notfound from "./NotFound";

import "./App.css";
import Scheduler from "./Scheduler";
import SchedulerPanel from "./SchedulerPanel";
//import { SIGNUP } from "./reducers";
const CustomRoute = ({
  isUserLogged,
  loggedUser,
  customAppointmentLink,
  MyComponent,
  ...rest
}) => {
  if (isUserLogged) {
    console.log(rest);
    return (
      <Route
        render={(props) => (
          <MyComponent
            loggedUser={loggedUser}
            customAppointmentLink={customAppointmentLink}
            {...props}
          />
        )}
        {...rest}
      />
    );
  } else {
    return <Redirect to="/" />;
  }
};

const mapStateToProps = (state) => {
  //let [loggedUser] = state.users.filter((x) => x.isLoggedIn === true);

  console.log(state.isUserLogged);
  return { isUserLogged: state.isUserLogged, loggedUser: state.loggedUser };
};

class App extends Component {
  render() {
    let name = this.props.loggedUser.name;

    let cust = name.replace(/\s+/g, "-").toLowerCase();

    let customAppointmentLink = `/appointments/${cust}-bookings`;

    console.log(customAppointmentLink);
    return (
      <>
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <CustomRoute
              exact
              path="/schedule"
              customAppointmentLink={customAppointmentLink}
              isUserLogged={this.props.isUserLogged}
              loggedUser={this.props.loggedUser}
              MyComponent={Schedule}
            />

            <Route exact path={customAppointmentLink} component={Scheduler} />
            <Route
              exact
              path={`${customAppointmentLink}/panel`}
              component={SchedulerPanel}
            />

            <Route
              exact
              path="/event_type/edit/"
              component={EditEventType}
              //render={(props) => <EditEventType {...props} />}
            />

            <Route exact path="/resetpassword" component={ResetPassword} />
            <Route
              exact
              path="/password/reset/:email/:token"
              render={({ match }) => (
                <UpdatePassword
                  email={match.params.email}
                  token={match.params.token}
                />
              )}
            />
          </Switch>
        </Router>
      </>
    );
  }
}

export default connect(mapStateToProps)(App);
