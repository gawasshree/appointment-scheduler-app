import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  Link,
  withRouter,
} from "react-router-dom";
import { LOGOUT, LOAD_EVENT_TYPES } from "./reducers";
import Scheduler from "./Scheduler";
import ShowEventTypes from "./EventType/ShowEventTypes";
import CreateEventType from "./EventType/CreateEventType";

const mapStateToProps = (state) => {
  let { loggedUser } = state;

  return { loggedUser };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
      console.log("trigger logout");
      // console.log("Set Login flag called");
      dispatch({ type: LOGOUT });
    },
    loadEventTypes: (events) => {
      console.log("trigger load events");

      dispatch({ type: LOAD_EVENT_TYPES, payload: events });
    },
  };
};

class Schedule extends Component {
  state = { event_types: [] };

  user = this.props.loggedUser.email;

  name = this.props.loggedUser.name;

  cust = this.props.customAppointmentLink;

  apiCall = () => {
    axios
      .get(`http://localhost:3001/api/event_types/${this.user}`)
      .then((res) => {
        console.log(res.status);
        if (res.status === 200) {
          this.setState({ event_types: res.data });
          this.props.loadEventTypes(res.data);
        }
      })
      .catch((err) => console.log);
  };

  updateEventTypes = () => {
    this.apiCall();
  };

  componentDidMount() {
    this.apiCall();
  }

  render() {
    console.log(this.cust);

    return (
      <>
        My Schedule
        <div style={{ textAlign: "right" }}>
          <button className="btn btn-danger" onClick={this.props.logout}>
            Logout
          </button>
        </div>
        <p>
          <Link to={this.props.customAppointmentLink}>
            {this.props.customAppointmentLink}
          </Link>
        </p>
        <div>
          <ShowEventTypes
            user={this.user}
            event_types={this.state.event_types}
          />
        </div>
        <div>
          <CreateEventType
            user={this.user}
            updateEventTypes={this.updateEventTypes}
          />
        </div>
      </>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
